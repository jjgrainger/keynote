// navigate between divs like a slideshow
(function($, window, undefined) {

    // Plugin is the plugins name
    // opts are the user supplied options
    $.fn.Keynote = function(opts) {

        // default plugin settings
        var defaults = {
            slides: '.slide',
            progress: '.progress',
            click: false,
            keys: true
        };

        // create options using the defaults and user supplied
        var options = $.extend(defaults, opts || {});

        var state = {
            total: 0,
            index: 0,
            current: ''
        };

        // pass the id of the slide to jump to it
        function goTo(slideId) {

            // set the new slide as current
            state.current = slideId;
            // set its index
            state.index = $(slideId).index();

            // scroll to that slide
            $('html, body').scrollTop($(state.current).offset().top);

            // update progress
            progress();
        };

        // update the progress bar
        function progress() {
            $(options.progress).find('.progress-bar').css({
                'width': (state.index/state.total)*100 + '%'
            });
        };

        $(window).keydown(function(e) {

            var key = e.which;
            var next = false;

            switch(key) {
                // previous control
                case 38 :
                    next = $(state.current).prev(options.slides).attr('id');
                    e.preventDefault();
                break;

                // next control
                case 40 :
                case 32 :
                    next = $(state.current).next(options.slides).attr('id');
                    e.preventDefault();
                break;
            }

            if(next) {
                goTo('#' + next);
            }
        });

        return this.each(function() {

            // set the state
            // total number of slides
            state.total = $(this).children(options.slides).length;

            // index of the first slide
            state.index = $(this).children(options.slides).first().index();
            state.current = '#' + $(this).children(options.slides).first().attr('id');

            // start the progress bar
            progress();
        });

    };
})(jQuery, window);
