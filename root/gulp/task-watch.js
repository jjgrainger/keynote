module.exports = function(gulp, config) {

    gulp.task('watch', function() {
        for(var src in config.gulp.watch) {
            gulp.watch(src, config.gulp.watch[src]);
        }
    });

};
