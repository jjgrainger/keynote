module.exports = function(gulp, config) {

    var concat = require('gulp-concat'),
        plumber = require('gulp-plumber'),
        uglify = require('gulp-uglify'),
        include = require("gulp-include");

    function handleError(err) {
      console.log(err.toString());
      this.emit('end');
    }

    gulp.task('scripts', function () {
      return gulp.src(config.gulp.scripts.src)
        .pipe(plumber({errorHandler: handleError }))
        // .pipe(concat('app.min.js'))
        .pipe(include())
        .pipe(uglify())
        .pipe(gulp.dest(config.gulp.scripts.dest))
        // .pipe(reload({stream:true}));
    });

};
