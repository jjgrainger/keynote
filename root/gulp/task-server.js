module.exports = function(gulp, config) {

    var connect = require('gulp-connect-php'),
        gutil = require('gulp-util');

    // Create server
    gulp.task('server', function() {
        connect.server({
            hostname: 'localhost',
            base: config.gulp.server.root,
            port: config.gulp.server.port,
            stdio: "ignore"
        }, function() {
            gutil.log('listening on http://localhost:' + config.gulp.server.port);
        });
    });
};
