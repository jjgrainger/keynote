// dependencies
var gulp = require('gulp'),
    config = require('./project.json');


// process sass styles -> gulp styles
require('./gulp/task-styles')(gulp, config);

// process javascript -> gulp scripts
require('./gulp/task-scripts')(gulp, config);

// run watch task -> gulp watch
require('./gulp/task-watch')(gulp, config);

// run watch task -> gulp server
require('./gulp/task-server')(gulp, config);

// build task to run before deployments
gulp.task('build', ['styles', 'scripts']);

gulp.task('serve', ['build', 'server']);

// run build and watch
gulp.task('default', ['serve', 'watch']);
