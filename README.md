# Keynote v1.0.0

> Create presentation slides with HTML and CSS.

## Requirements

* You have [node](http://nodejs.org) installed
* You have [grunt-cli](http://gruntjs.com/getting-started) installed
* You have [grunt-init](http://gruntjs.com/project-scaffolding) installed

## Installation

To innstall the scaffold into the `.grunt-init` folder in your home directory, run the following in your terminal.

```
$ git clone git@bitbucket.org:venncreative/keynote.git ~/.grunt-init/keynote
```

_(Windows users, see [the documentation](http://gruntjs.com/project-scaffolding#installing-templates) for the correct destination directory path)_

## Usage

Create a project folder, then initialize the scaffold with `grunt-init keynote`

```
$ grunt-init keynote
```

## Notes

* Licensed under the [MIT License](LICENSE.md)
* Maintained under the [Semantic Versioning Guide](http://semver.org)
